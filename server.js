var express = require('express');
var app = express();

app.use(express.static('./public'));
app.get('/', function(req,res){
  res.sendfile('./public/index.html')
})

app.listen(3001);

console.log('Server running at http://localhost:3001')
